import numpy as np

def clima(datos):

    datos_array_plano = np.array(datos,dtype='object') 

    datos_array = datos_array_plano.reshape(108,7)

    datos_array_nodate =np.delete(datos_array, 0, 1) 


    promedios = np.mean(datos_array_nodate, axis = 0)
    minimos = datos_array_nodate.min(axis = 0)
    maximos = datos_array_nodate.max(axis = 0)

    filas_temp_min = []
    filas_temp_max = []
    filas_precip_min = []
    filas_precip_max = []
    filas_max_dias_sol = []

    for i in range(108):
        if datos_array_nodate[i][0] == minimos[0]:
            filas_temp_min.append(i)
        if datos_array_nodate[i][1] == maximos[1]:
            filas_temp_max.append(i)
        if datos_array_nodate[i][2] == minimos[2]:
            filas_precip_min.append(i)
        if datos_array_nodate[i][2] == maximos[2]:
            filas_precip_max.append(i)
        if datos_array_nodate[i][5] == maximos[5]:
            filas_max_dias_sol.append(i)

    fechas_temp_min = []
    fechas_temp_max = []
    fechas_precip_max = []
    fechas_precip_min = []
    fechas_max_dias_sol = []

    for item in filas_temp_min:
        fechas_temp_min.append(datos_array[item][0])

    for item in filas_temp_max:
        fechas_temp_max.append(datos_array[item][0])

    for item in filas_precip_min:
        fechas_precip_min.append(datos_array[item][0])

    for item in filas_precip_max:
        fechas_precip_max.append(datos_array[item][0])

    for item in filas_max_dias_sol:
        fechas_max_dias_sol.append(datos_array[item][0])

    return fechas_temp_min, fechas_temp_max, fechas_precip_min, fechas_precip_max, fechas_max_dias_sol