#from numpy import random # Esta solución se puede lograr con otros métodos y otras librerías

def generar_baraja(tipos_cartas, n_palos):
    # Se crean las copias de los tipos según la cantidad de palos
    baraja_cartas = tipos_cartas * n_palos
    # Barajar cartas
    random.shuffle(baraja_cartas)
    # Convertir a tuplas

    #return 

""" 
nombres = ['Andrés', 'Juliana', 'María', 'Juan']
respuesta = nombres * 4
print(respuesta) """