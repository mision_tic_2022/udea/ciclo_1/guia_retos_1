import numpy as np
def balotera(balotas):
    np.random.shuffle(balotas)
    d = {
        'B': 0,
        'I': 0,
        'N': 0,
        'G': 0,
        'O': 0
    }
    for balota in balotas:
        if d['B'] >= 5 and d['I'] >= 5 and d['G'] >= 5 and d['O'] >= 5 and d['N'] >= 4:
            break
        d[balota[0]] += 1
    print(d)
    return balotas

['B1', 'B15', 'I20']