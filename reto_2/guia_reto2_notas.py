'''
Cuadro de honor: 
*3 estudiantes con mejores notas
*En caso de empate se colocan las cédulas
*Calcular el promedio general del grupo

*Un grupo se representa por una lista de diccionarios donde cada diccionario es un estudiante:
    [ {}, {}, {} ]
*Cada estudiante se representa por un diccionario. Estructura del diccionario:
    estudiante = {
        'cedula': '',
        'nombre': '',
        'nota_fundamentos': 0.0
    }

*Mostrar/retornar un diccionario con los tres primeros puestos. Estructura del diccionario
    cuadro_honor = {
        1: ['12345', '4321'],
        2: [''],
        3: []
    }

NOTA:
*La función recibe la lista como parámetro
*La función debe retornar una tupla.
    (promedio_grupo, cuadro_honor)
'''

def calcular_promedio_y_cuadro_honor(grupo: list)->dict:
    #Variable que representa el promedio general del grupo
    promedio = 0
    #Variable que representa los mejores estudiantes
    cuadro_honor = {
        1: [],
        2: [],
        3: []
    }
    #Iterar lista 'grupo'
    for estudiante in grupo:
        nota_fundamentos = estudiante['nota_fundamentos']
        #Sumar notas
        promedio += nota_fundamentos
        for est in grupo:
            pass
    
    #Calcular promedio
    can_notas = len(grupo)
    promedio = promedio / can_notas

    return (promedio, cuadro_honor)
