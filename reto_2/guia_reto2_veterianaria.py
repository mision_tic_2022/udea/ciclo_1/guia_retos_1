'''
Especies: 
    Caninos, felinos, equinos, bovinos y roedores.

Si un animal se clasifica como adulto y es canino, felino, equino o bovino
entonces se aplica un acompañamiento en salud.

Adultos mayores:
    Canino o felino -> >= 9
    Equino o bovino -> >= 16

Listas paralelas:
    nombres: [nombres de los pacientes]
    tipos: [canino, felino, equino, bovino, roedor]
    edades: []
    pesos: []#pesos de cada paciente

respuesta = {
    '1': [],
    '2': []'
    ...
}

'''

# POR FAVOR LEA TODAS LAS INDICACIONES SUMINISTRADAS EN EL
# ENUNCIADO ANTES DE EMPEZAR A IMPLEMENTAR SU SOLUCIÓN

# NO MODIFICAR LOS NOMBRES, PARÁMETROS DE ENTRADA NI RETORNOS DE LAS FUNCIONES
# USE LOS MISMOS NOMBRES DE LOS RETORNOS PARA NOMBRAR LOS RESULTADOS QUE SU FUNCIÓN DEBE GENERAR


def veterinaria(nombres, tipos, edades, pesos):
    # ACÁ INICIA LA FUNCIÓN VETERINARIA
    # (En este espacio debe poner el código necesario para crear los diccionarios y promedios pedidos)
    diccionario = {}
    beneficiarios_can_fel = {}
    beneficiarios_equ_bov = {}
    promedio = 0.0
    promedio_can_fel = 0.0
    promedio_equ_bov = 0.0

    firstDic(nombres, tipos, edades, pesos, diccionario)

    secondDic(nombres, tipos, edades, pesos, beneficiarios_can_fel)
    # 3
    thirdDic(nombres, tipos, edades, beneficiarios_equ_bov)
    # promedios
    promedio_can_fel = Average(
        tipos, edades, diccionario, promedio, "canino", "felino", 9)

    promedio_equ_bov = Average(
        tipos, edades, diccionario, promedio, "bovino",  "equino", 16)

    print(promedio_equ_bov)
    return diccionario, beneficiarios_can_fel, beneficiarios_equ_bov, promedio_can_fel, promedio_equ_bov


def Average(tipos, edades, diccionario, promedio, firstName, secondName, edad):
    contador = 0
    promedio = 0
    for x in range(len(diccionario)):
        if(tipos[x] == firstName or tipos[x] == secondName):
            if ((edades[x] >= edad)):
                contador = contador+1
                promedio = promedio+edades[x]
    if(contador != 0):
        promedio = promedio/contador
    else:
        promedio = None
    return promedio


def thirdDic(nombres, tipos, edades, beneficiarios_equ_bov):
    index_2 = 0
    for x in range(len(nombres)):
        if (edades[x] >= 16 and (tipos[x] == "bovino" or tipos[x] == "equino")):
            index_2 = index_2 + 1
            beneficiarios_equ_bov[str(index_2)] = [nombres[x]]


def secondDic(nombres, tipos, edades, pesos, beneficiarios_can_fel):
    index_1 = 0
    for x in range(len(nombres)):
        if ((edades[x] >= 9 and (tipos[x] == "canino" or tipos[x] == "felino"))):
            index_1 = index_1+1
            beneficiarios_can_fel[str(index_1)] = [nombres[x], tipos[x], pesos[x]]
        if ((edades[x] >= 16 and (tipos[x] == "bovino" or tipos[x] == "equino"))):
            index_1 = index_1+1
            beneficiarios_can_fel[str(index_1)] = [nombres[x], tipos[x], pesos[x]]


def firstDic(nombres, tipos, edades, pesos, diccionario):
    for x in range(len(nombres)):
        diccionario[str(x+1)] = [nombres[x], tipos[x], edades[x], pesos[x]]
