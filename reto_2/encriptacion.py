mensaje = "¡HOLA MUNDO!"
def encriptador(mensaje):
    diccionario = {
        'A': "Z",
        'B': "Y",
        'C': "X",
        'D': "V",
        'E': "U",
        'F': "T",
        'G': "S",
        'H': "R",
        'I': "Q",
        'J': "P",
        'K': "O",
        'L': "N",
        'M': "M",
        'N': "L",
        'O': "K",
        'P': "J",
        'Q': "J",
        'R': "I",
        'S': "H",
        'T': "G",
        'U': "F",
        'V': "E",
        'X': "D",
        'Y': "C",
        'Z': "B",
        ' ': "A",
        '!': "#",
        "¡": "+",
    }
    clave={}
    encriptado = ""
    for i in mensaje:
        for x in diccionario:
            if i == x:
                clave[i]= diccionario[x]
                encriptado += diccionario[x]
    print(encriptado, clave)
    return encriptado, clave

encriptador(mensaje)

encriptado = "+RKNZAMFLVK#"
clave = {'¡': '+', 'H': 'R', 'O': 'K', 'L': 'N', 'A': 'Z', ' ': 'A', 'M': 'M', 'U': 'F', 'N': 'L', 'D': 'V', '!': '#'}
def desencriptador(encriptado, clave):
    desencriptado = ""
    for i in encriptado:
        for x in clave:
            if i == clave[x]:
                desencriptado += x
    print(desencriptado)
    return desencriptado

desencriptador(encriptado, clave)

