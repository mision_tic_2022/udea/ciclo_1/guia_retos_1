'''
*Modificar una materia del pensum
*Lista de semestres
*Un semestre es un diccionario de materias, con la siguiente estructura:
    semestre = {
        '1234': materia
    }
    Donde '1234' es el código de la materia
*Una materia es un diccionario con la siguiente estructura:
    materia = {
        'nombre': '',
        'creditos': 0
    }
    
'''


# POR FAVOR LEA TODAS LAS INDICACIONES SUMINISTRADAS EN EL
# ENUNCIADO ANTES DE EMPEZAR A IMPLEMENTAR SU SOLUCIÓN

'''
Modificar los valores de la variable 'mensaje' por los que indica
el pdf
'''

# NO MODIFICAR EL NOMBRE, PARÁMETROS O RETORNO DE LA FUNCIÓN
def modificar_materia(pensum, semestre, materia, nombre, creditos):
    mensaje = ''
    #Obtener la cantidad de semestres recibidas en el pensum
    cant_semestres = len(pensum)
    #Validar que exista el semestre
    if semestre > 0 and semestre <= cant_semestres:
        item_semestre = pensum[semestre-1]
        if len(item_semestre) > 0:
            #Validar si la materia(codigo materia) existe en el semestre
            if materia in item_semestre:
                pass
            else:
                mensaje = 'La materia no existe en este semestre'
        else:
            mensaje = 'No existen materias en el semestre indicado'
        
    else:
        mensaje = 'Ingrese un semestre válido'

    return mensaje


pensum = [
    {
        '0123': {'nombre': 'intro a la ing', 'créditos': 2},
        '4567': {'nombre': 'inglés', 'créditos': 1}
    },
    {}, {}
]

