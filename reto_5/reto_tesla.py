#NO ELIMINAR LAS SIGUIENTES IMPORTACIONES, sirven para el funcionamiento de las librería csv y json respectivamente
import csv, json
import numpy as np

def solucion():
    with open('TESLA.csv') as tesla_csv:
        #Preparamos el archivo para iterarlo
        tesla_reader = csv.reader(tesla_csv)
        
        #Definimos el encabezado del archivo que posteriormente vamos a crear
        analisis_header = ['Fecha',	'Comportamiento de la accion', 'Ajuste Cuadratico de Close']
        
        #Creamos una lista donde guardaremos el contenido del archivo analisis_archivo.csv (Será una lista de listas, donde cada elemento es una fila del archivo  a crear)
        analisis_content = list()
        
        #Añadimos el encabezado como primer elemento
        analisis_content.append(analisis_header)
        
        #Iteremos el archivo tesla.csv pero antes, saltémonos el encabezado
        next(tesla_reader)
        for i, row_tesla in enumerate(tesla_reader):
            """
            Columnas de tesla.csv:
            0. Date: Fecha del dato
            1. Open: Precio de apertura de la acción en la bolsa
            2. High: Precio más alto durante el día
            3. Low: Precio más bajo durante el día
            4. Close: Precio de cierre de la acción
            5. Adj Close: Precio de cierre ajustado de la acción
            6. Volume: Volumen de acciones transadas durante el día
            """
            if i == 0:
                #Creamos el diccionario que posteriormente lo convertiremos en JSON
                detalles_dict = {
                    "date_lowest_open": row_tesla[0],
                    "lowest_open": float(row_tesla[1]),
                    "date_highest_close": row_tesla[0],
                    "highest_close": float(row_tesla[4]),
                    "mean_volume": float(row_tesla[6]),
                    "date_greatest_difference": row_tesla[0],
                    "greatest_difference": abs(float(row_tesla[3]) - float(row_tesla[2]))
                }
                
            else:
                #Veamos si hay una fecha en la que hay un menor precio de open la acción
                if detalles_dict['lowest_open'] > float(row_tesla[1]):
                    detalles_dict['date_lowest_open'] = row_tesla[0]
                    detalles_dict['lowest_open'] = float(row_tesla[1])
                    
                #Veamos si hay una fecha en la que hay un mayor precio de la acción
                if detalles_dict['highest_close'] < float(row_tesla[4]):
                    detalles_dict['date_highest_close'] = row_tesla[0]
                    detalles_dict['highest_close'] = float(row_tesla[4])
                    
                #Veamos si hay una fecha en la que hay un mayor |Low - High|
                if detalles_dict['greatest_difference'] < abs(float(row_tesla[3]) - float(row_tesla[2])):
                    detalles_dict['date_greatest_difference'] = row_tesla[0]
                    detalles_dict['greatest_difference'] = abs(float(row_tesla[3]) - float(row_tesla[2]))
                    
                #Cálculo para la media
                detalles_dict['mean_volume'] += float(row_tesla[6])

            #La primera columna siempre es la fecha que se está leyendo
            row_analisis = [row_tesla[0]]
            
            #La segunda columna es un concepto (Recuerde que el reader lo asume como str)
            if float(row_tesla[4]) - float(row_tesla[1]) > 0:
                row_analisis.append('SUBE')
            elif float(row_tesla[4]) - float(row_tesla[1]) < 0:
                row_analisis.append('BAJA')
            else:
                row_analisis.append('ESTABLE')
                
            #La tercera columna es la operación sqrt((CLOSE - AdjCLOSE) ** 2)
            row_analisis.append(sqrt((float(row_tesla[4]) - float(row_tesla[5])) ** 2))
            
            #Guardamos el renglon en la lista que guarda los renglones de analisis_archivo.csv
            analisis_content.append(row_analisis)
    
    detalles_dict['mean_volume'] /= len(analisis_content) - 1
            
    #Creamos el archivo analisis_archivo.csv
    with open('analisis_archivo.csv', 'w', newline='') as analisis_file:
        #Preparamos el archivo para escribirlo
        analisis_writer = csv.writer(analisis_file, delimiter = '\t')
        analisis_writer.writerows(analisis_content)
        
    #Aquí crear el archivo detalles.json