def solucion():
    path = ''
    
    #Abrimos JandJ.csv como de lectura
    with open(path + 'JandJ.csv') as JandJ_csv:
        #Preparamos el archivo para iterarlo
        JandJ_reader = csv.reader(JandJ_csv)
        
        #Definimos el encabezado del archivo que posteriormente vamos a crear
        analisis_header = ['Fecha analizada',	'Comportamiento de la accion', 'abs Diferencia Close-Open']
        
        #Creamos una lista donde guardaremos el contenido del archivo analisis_archivo.csv (Será una lista de listas, donde cada elemento es una fila del archivo  a crear)
        analisis_content = list()
        
        #Añadimos el encabezado como primer elemento
        analisis_content.append(analisis_header)
        
        #Iteremos el archivo JandJ.csv pero antes, saltémonos el encabezado
        next(JandJ_reader)
        s=0
        for i, row_JandJ in enumerate(JandJ_reader):
            s+=1
            """
            Columnas de JandJ.csv:
            0. Date: Fecha del dato
            1. Open: Precio de apertura de la acción en la bolsa
            2. High: Precio más alto durante el día
            3. Low: Precio más bajo durante el día
            4. Close: Precio de cierre de la acción
            5. Adj Close: Precio de cierre ajustado de la acción
            6. Volume: Volumen de acciones transadas durante el día
            """
            if i == 0:
                #Creamos el diccionario que posteriormente lo convertiremos en JSON
                detalles_dict = {
                    "date_lowest_volume": row_JandJ[0],
                    "lowest_volume": int(row_JandJ[6]),
                    "date_highest_volume": row_JandJ[0],
                    "highest_volume": int(row_JandJ[6]),
                    "mean_volume": float(row_JandJ[6]),
                    "date_greatest_difference": row_JandJ[0],
                    "greatest_difference": abs(float(row_JandJ[4]) - float(row_JandJ[1]))
                }
                
            else:
                #Veamos si hay una fecha en la que hay un menor volume de la acción
                if detalles_dict['lowest_volume'] > float(row_JandJ[6]):
                    detalles_dict['date_lowest_volume'] = row_JandJ[0]
                    detalles_dict['lowest_volume'] = float(row_JandJ[6])
                    
                #Veamos si hay una fecha en la que hay un mayor volume de la acción
                if detalles_dict['highest_volume'] < float(row_JandJ[6]):
                    detalles_dict['date_highest_volume'] = row_JandJ[0]
                    detalles_dict['highest_volume'] = float(row_JandJ[6])
                    
                #Veamos si hay una fecha en la que hay una diferencia Close - Open más grande
                if detalles_dict['greatest_difference'] < abs(float(row_JandJ[4]) - float(row_JandJ[1])):
                    detalles_dict['date_greatest_difference'] = row_JandJ[0]
                    detalles_dict['greatest_difference'] = abs(float(row_JandJ[4]) - float(row_JandJ[1]))
                    
                #Cálculo para la media
                detalles_dict['mean_volume'] += float(row_JandJ[6])
                    
            #La primera columna siempre es la fecha que se está leyendo
            row_analisis = [row_JandJ[0]]
            
            #La segunda columna es un concepto (Recuerde que el reader lo asume como str)
            if float(row_JandJ[4]) - float(row_JandJ[1]) > 0:
                row_analisis.append('SUBE')
            elif float(row_JandJ[4]) - float(row_JandJ[1]) < 0:
                row_analisis.append('BAJA')
            else:
                row_analisis.append('ESTABLE')
                
            #La tercera columna es la operación |Close - Open|
            row_analisis.append(abs(float(row_JandJ[4]) - float(row_JandJ[1])))
            
            #Guardamos el renglon en la lista que guarda los renglones de analisis_archivo.csv
            analisis_content.append(row_analisis)
            
    detalles_dict['mean_volume'] /= len(analisis_content) - 1
    print(len(analisis_content) - 1 == s)
    
    #Aquí crear el archivo analisis_archivo.csv
    #...
    #Aquí crear el archivo detalles.json
    #...