import json
import csv

def solucion():
    with open('GLOBANT.csv') as globant_csv:
        globant_reader = csv.reader(globant_csv)

        header = ['Fecha', 'Comportamiento de la accion', 'Punto medio HIGH-LOW']

        analisis_contenido = list()
        analisis_contenido.append(header)
        dict_ = dict()
        next(globant_reader)
        for i, row in enumerate(globant_reader):
            if i == 0:
                dict_ = {
                    "date_lowest_price": row[0],
                    "lowest_price":float(row[3]),
                    "date_highest_price": row[0],
                    "highest_price": float(row[2]),
                    "cantidad_veces_sube": 0,
                    "cantidad_veces_baja": 0,
                    "cantidad_veces_estable":0
                }

            else:
                if (dict_["lowest_price"] > float(row[3])):
                    dict_["date_lowest_price"] = row[0]
                    dict_["lowest_price"] = float(row[3])
                if (dict_["highest_price"] < float(row[2])):
                    dict_["date_highest_price"] = row[0]
                    dict_["highest_price"] = float(row[2])

            row_analisis = [row[0]]

            if float(row[4]) - float(row[1]) > 0:
                row_analisis.append("SUBE")
                dict_["cantidad_veces_sube"] +=1
            elif float(row[4]) - float(row[1]) < 0:
                row_analisis.append("BAJA")
                dict_["cantidad_veces_baja"] +=1
            else:
                row_analisis.append("ESTABLE")
                dict_["cantidad_veces_estable"] +=1

            row_analisis.append((float(row[2])-float(row[3]))/2)
            analisis_contenido.append(row_analisis)
        with open('analisis_archivo.csv','w', newline='') as analisis_file:
            analisis_write = csv.writer(analisis_file, delimiter='\t')
            analisis_write.writerows(analisis_contenido)

        with open('detalles.json','w') as detalle_json:
            json.dump(dict_, detalle_json)

solucion()
    