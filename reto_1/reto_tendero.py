def solucion():
    faltan_productos = 'S'
    subtotal = 0
    while faltan_productos == 'S':
        #Obtener precio del producto
        valor_unitario = int(input('Ingrese el valor unitario'))
        cantidad_productos = int(input('Ingrese la cantidad de productos a registrar'))
        tiene_iva = input('¿El producto cuenta con IVA? S/N').upper()
        if tiene_iva == 'S':
            print('IVA INCLUIDO')
            subtotal += (valor_unitario*cantidad_productos) * 1.19
        else:
            print('PRODUCTO SIN IVA')
            subtotal += (valor_unitario*cantidad_productos)
        #upper convierte a mayúscula
        faltan_productos = input('¿Faltan productos por cobrar? S/N').upper()
        #Aquí imprimir el subtotal
    
    print(f'Aquí va el mensaje-> TOTAL A COBRAR {subtotal}')