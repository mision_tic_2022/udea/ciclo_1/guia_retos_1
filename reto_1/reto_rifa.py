
def solucion(b,n):
    #ACÁ INICIA LA FUNCIÓN SOLUCIÓN (En este espacio debes entregar tu solución)
    cantidad_intentos= 0
    while True:
        num= int( input("Ingrese un número entre 0 y "+str(b)+": "))
        if num < 0 or num > b:
            print("¡Te saliste del intervalo!")
        elif num > n:
            cantidad_intentos += 1
            print ("¡Ups! Te pasaste")
        elif num < n:
            cantidad_intentos += 1
            print("¡Ups! Estás por debajo")
        elif num == n:
            cantidad_intentos += 1
            print('¡Ganaste!')
            #Imprimir mensajes requeridos
            #break rompe el ciclo
            break
        
        '''num > n and num >= 0 and num <= b:
            print ("¡Ups! Te pasaste")
            cantidad_intentos+=1
            '''