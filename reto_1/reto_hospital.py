def solucion(edad, peso):
    if edad <5 or edad >17:
        print('No cumple con la edad para la campaña del Hospital')
    else:
        a: bool = False
        b: bool = False
        c: bool = False
        estado_Nutricional: str = " "
        ganar_peso: int = 0
        perder_peso: int = 0 
        mantener_peso: int = 0
        ganancia_Peso_A_diaria: float = 101.9
        perdida_Peso_B_diaria: float = -31.04
        mantener_Peso_C_diaria: float = 2.6
        dias: int = 0
        if edad >= 5 and edad <= 10:
            if peso < 16:
                a = True
                estado_Nutricional = "A"
                ganar_peso = (22 - peso) * 1000 
                dias = round (ganar_peso / ganancia_Peso_A_diaria) 
                print(f'El estado nutricional del paciente es {estado_Nutricional} y se requieren {dias} dias de dieta para que alcance un peso saludable')

            elif peso > 28:
                b = True
                estado_Nutricional = "B"
                perder_peso = (24 - peso) * 1000 
                dias = round (perder_peso / perdida_Peso_B_diaria) 
                print(f'El estado nutricional del paciente es {estado_Nutricional} y se requieren {dias} dias de dieta para que alcance un peso saludable')
            else:
                c = True
                estado_Nutricional = "C"
                mantener_peso = (28 - peso) * 1000 
                dias = round (mantener_peso / mantener_Peso_C_diaria) 
                print(f'El estado nutricional del paciente es {estado_Nutricional} y se requieren {dias} dias de dieta para que alcance el peso máximo')
            

        if edad > 10 and edad <= 13:
            if peso < 30:
                a = True
                estado_Nutricional = "A"
                ganar_peso = (32 - peso) * 1000 
                dias = round (ganar_peso / ganancia_Peso_A_diaria) 
                print(f'El estado nutricional del paciente es {estado_Nutricional} y se requieren {dias} dias de dieta para que alcance un peso saludable')
            elif peso > 50:
                b = True
                estado_Nutricional = "B"
                perder_peso = (43 - peso) * 1000 
                dias = round (perder_peso / perdida_Peso_B_diaria) 
                print(f'El estado nutricional del paciente es {estado_Nutricional} y se requieren {dias} dias de dieta para que alcance un peso saludable')
            else:
                c = True
                estado_Nutricional = "C"
                mantener_peso = (50 - peso) * 1000 
                dias = round (mantener_peso / mantener_Peso_C_diaria) 
                print(f'El estado nutricional del paciente es {estado_Nutricional} y se requieren {dias} dias de dieta para que alcance el peso máximo')
            

        while edad > 13 and edad <= 17:
            if peso < 51:
                a = True
                estado_Nutricional = "A"
                ganar_peso = (56 - peso) * 1000 
                dias = round (ganar_peso / ganancia_Peso_A_diaria) 
                print(f'El estado nutricional del paciente es {estado_Nutricional} y se requieren {dias} dias de dieta para que alcance un peso saludable')
            elif peso > 63:
                b = True
                estado_Nutricional = "B"
                perder_peso = (58 - peso) * 1000 
                dias = round (perder_peso / perdida_Peso_B_diaria) 
                print(f'El estado nutricional del paciente es {estado_Nutricional} y se requieren {dias} dias de dieta para que alcance un peso saludable')
            else:
                c = True
                estado_Nutricional = "C"
                mantener_peso = (63 - peso) * 1000 
                dias = round (mantener_peso / mantener_Peso_C_diaria) 
                print(f'El estado nutricional del paciente es {estado_Nutricional} y se requieren {dias} dias de dieta para que alcance el peso máximo')
            
                

#solucion(edad=float(input('Indicar la edad del paciente: ' )), peso = float(input('Indicar el peso del paciente en Kg: '   )))

