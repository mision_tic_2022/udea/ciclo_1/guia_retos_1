def solucion():
    print('¡Bienvenido! En esta aplicación los estudiantes podrán gestionar las notas de su materia.')
    nom_student = input('Por favor ingrese su nombre: ').capitalize()
    materia = input('Ingrese el nombre de la materia: ').capitalize()
    ciclo = True
    nota = []
    porcentaje = []
    while ciclo == True:
        grades = float(input('Ingrese la nota obtenida: '))
        nota.append(grades)
        percen = (int(input('Ingrese el porcentaje de la nota: '))) / 100
        porcentaje.append(percen)
        if sum(porcentaje) < 1:
            falta_nota = input('¿Falta añadir notas? S/N: ')
            if falta_nota.upper() == 'N':
                break
        elif sum(porcentaje) == 1:
            break
        elif sum(porcentaje) + percen > 1:
            print('El porcentaje evaluado de una materia no puede ser mayor a 100')
            porcentaje.remove(porcentaje[-1])
            nota.remove(nota[-1])
    reporte = 0
    for i in range(len(nota)):
        reporte += nota[i] * porcentaje[i]
    aprobacion = ''
    if reporte < 3:
        aprobacion = 'reprobado'
    else:
        aprobacion = 'aprobado'
    reporte_aprox = round(reporte, 2)
    print(f'El estudiante {nom_student} cursó la materia {materia} y obtuvo {reporte_aprox} resultando en {aprobacion}')

solucion()