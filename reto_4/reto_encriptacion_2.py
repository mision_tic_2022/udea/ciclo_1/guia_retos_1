get_alfabeto = lambda: {'A': 0, 'B': 1, 'C': 2, 'D': 3, 'E': 4, 'F': 5, 'G': 6, 'H': 7, 'I': 8, 'J': 9, 'K': 10, 'L': 11,
                            'M': 12, 'N': 13, 'Ñ': 14, 'O': 15, 'P': 16, 'Q': 17, 'R': 18, 'S': 19, 'T': 20, 'U': 21, 'V': 22,
                            'W': 23, 'X': 24, 'Y': 25, 'Z': 26}


def encriptar_caracter(caracter, b):
    alfabeto = get_alfabeto()
    a = int(alfabeto[caracter])
    caracter_encriptado = (a+b) % 27
    return caracter_encriptado


def encriptar(mensaje, b):
    alfabeto = get_alfabeto()

    num_cor = {}
    for i in alfabeto:
        num_cor[alfabeto[i]] = i

    mensaje = mensaje.upper()

    mensaje_frac = []
    for i in mensaje:
        mensaje_frac.append(i)

    encriptado_1 = []
    for j in range(len(mensaje_frac)):
        if not mensaje_frac[j] in get_alfabeto():
            encriptado_1.append(mensaje_frac[j])
        else:
            encriptado_1.append(
                num_cor[encriptar_caracter(mensaje_frac[j], b)])

    mensaje_encriptado = ''.join(encriptado_1)
    # for h in encriptado_1:
    #mensaje_encriptado += h
    return mensaje_encriptado


def desencriptar_caracter(caracter_encriptado, b):
    alfabeto = get_alfabeto()
    phi = alfabeto[caracter_encriptado]
    caracter_desencriptado = (phi-b) % 27
    return caracter_desencriptado


def desencriptar(mensaje_encriptado, b):
    alfabeto = get_alfabeto()
    num_cor = {}
    for i in alfabeto:
        num_cor[alfabeto[i]] = i

    encrip_frac = []
    for i in mensaje_encriptado:
        encrip_frac.append(i)

    desencriptado_1 = []
    for j in range(len(encrip_frac)):
        if not encrip_frac[j] in get_alfabeto():
            desencriptado_1.append(encrip_frac[j])
        else:
            desencriptado_1.append(
                num_cor[desencriptar_caracter(encrip_frac[j], b)])

    caracter_desencriptado = ''.join(desencriptado_1)
    # for h in desencriptado_1:
    #caracter_desencriptado += h
    return caracter_desencriptado


resp_enc = desencriptar(encriptar(['h','o',2], 7), 7)
print(resp_enc)
