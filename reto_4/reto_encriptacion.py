
abecedario = lambda : ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
       'N', 'Ñ', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']

def 𝑒𝑛𝑐𝑟𝑖𝑝𝑡𝑎𝑟_𝑐𝑎𝑟𝑎𝑐𝑡𝑒𝑟(𝑐𝑎𝑟𝑎𝑐𝑡𝑒𝑟,  𝑏):
    abc = abecedario()
    a = abc.index(caracter)
    index = (a + 𝑏) % 27
    caracter_encriptado = abc[index]
    return caracter_encriptado


def 𝑒𝑛𝑐𝑟𝑖𝑝𝑡𝑎𝑟(mensaje,  𝑏):
    abc = abecedario()
    ec, m = encriptar_caracter, mensaje
    mensaje_encriptado = ''.join(
        ec(char, b) if char in abc else char for char in m)
    return mensaje_encriptado


def 𝑑𝑒𝑠𝑒𝑛𝑐𝑟𝑖𝑝𝑡𝑎𝑟_𝑐𝑎𝑟𝑎𝑐𝑡𝑒𝑟(𝑐𝑎𝑟𝑎𝑐𝑡𝑒𝑟_𝑒𝑛𝑐𝑟𝑖𝑝𝑡𝑎𝑑𝑜,  b):
    abc = abecedario()
    ϕ = abc.index(caracter_encriptado)
    caracter_desencriptado = abc[(ϕ - b) % 27]
    return caracter_desencriptado


def 𝑑𝑒𝑠𝑒𝑛𝑐𝑟𝑖𝑝𝑡𝑎𝑟(𝑚𝑒𝑛𝑠𝑎𝑗𝑒_𝑒𝑛𝑐𝑟𝑖𝑝𝑡𝑎𝑑𝑜,  𝑏):
    abc = abecedario()
    dc, me = desencriptar_caracter, mensaje_encriptado
    mensaje_desencriptado = ''.join(
        dc(char, b) if char in abc else char for char in me)
    return mensaje_desencriptado


solucion = desencriptar(encriptar('U DE A', 7), 7)
print( encriptar('U DE A', 7) )
print(solucion)
